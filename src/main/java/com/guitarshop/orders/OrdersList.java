package com.guitarshop.orders;

import java.util.List;

public class OrdersList {

    private List<Order> orders;

    public OrdersList(List<Order> orders) {
        this.orders = orders;
    }

    public OrdersList() {
    }

    public List<Order> getOrders() {
        return orders;
    }

    public void setOrders(List<Order> orders) {
        this.orders = orders;
    }
}
