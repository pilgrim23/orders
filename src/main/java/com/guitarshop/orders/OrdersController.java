package com.guitarshop.orders;

import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.Date;

@RestController
@RequestMapping("/api/orders")
public class OrdersController {

    OrdersService ordersService;

    public OrdersController(OrdersService ordersService) {
        this.ordersService = ordersService;
    }

    @GetMapping
    public ResponseEntity<OrdersList> getOrders() {
        OrdersList orders = ordersService.getOrders();
        if (orders == null) return ResponseEntity.noContent().build();
        return ResponseEntity.ok(orders);
    }

    @PostMapping
    public Order addOrder(@RequestBody Order order) {
        return ordersService.addOrder(order);
    }

    @GetMapping("/{orderId}")
    public ResponseEntity<Order> getOrder(@PathVariable Long orderId) {
        Order order = ordersService.getOrderById(orderId);
        if (order == null) return ResponseEntity.noContent().build();
        return ResponseEntity.ok(order);
    }

    @PatchMapping("/{orderId}")
    public ResponseEntity<Order> updateOrderById(@PathVariable Long orderId, @RequestBody UpdateOrderRequest request) {
        String shipAddress = request.getShipAddress() == null ? "" : request.getShipAddress();
        String billAddress = request.getBillingAddress() == null ? "" : request.getBillingAddress();
        Date shipDate = request.getShipDate();
        Order order = ordersService.updateOrderById(orderId, shipAddress, billAddress, shipDate);
        if (order == null) return ResponseEntity.noContent().build();

        return ResponseEntity.ok(order);
    }

    @DeleteMapping("/{orderId}")
    public ResponseEntity<Order> deleteOrder(@PathVariable Long orderId) {
        try {
            ordersService.deleteOrder(orderId);
        } catch (OrderNotFoundException e) {
            return ResponseEntity.noContent().build();
        }
        return ResponseEntity.accepted().build();
    }
}
