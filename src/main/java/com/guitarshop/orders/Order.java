package com.guitarshop.orders;

import javax.persistence.*;
import java.util.Date;

@Entity
@Table(name = "orders")
public class Order {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private long orderId;
    private long customerId; // Foreign Key
    private double shipAmount; // calculated
    private double taxAmount; // calculated
    private Date orderDate; // on creation
    private Date shipDate; // on shipment
    private String shipAddress; // where to ship
    private String billingAddress; // where to bill

    public Order(long customerId, double shipAmount, double taxAmount, String shipAddress) {
        this.customerId = customerId;
    }

    public Order(long customerId, double shipAmount, double taxAmount, String shipAddress, String billingAddress) {
        this.customerId = customerId;
    }

    public Order() {
    }

    public long getOrderId() {
        return orderId;
    }

    public void setOrderId(long orderId) {
        this.orderId = orderId;
    }

    public long getCustomerId() {
        return customerId;
    }

    public void setCustomerId(long customerId) {
        this.customerId = customerId;
    }

    public double getShipAmount() {
        return shipAmount;
    }

    public void setShipAmount(double shipAmount) {
        this.shipAmount = shipAmount;
    }

    public double getTaxAmount() {
        return taxAmount;
    }

    public void setTaxAmount(double taxAmount) {
        this.taxAmount = taxAmount;
    }

    public Date getOrderDate() {
        return orderDate;
    }

    public void setOrderDate(Date orderDate) {
        this.orderDate = orderDate;
    }

    public Date getShipDate() {
        return shipDate;
    }

    public void setShipDate(Date shipDate) {
        this.shipDate = shipDate;
    }

    public String getShipAddress() {
        return shipAddress;
    }

    public void setShipAddress(String shipAddress) {
        this.shipAddress = shipAddress;
    }

    public String getBillingAddress() {
        return billingAddress;
    }

    public void setBillingAddress(String billingAddress) {
        this.billingAddress = billingAddress;
    }
}
