package com.guitarshop.orders;

import java.util.Date;

public class UpdateOrderRequest {

    private String shipAddress;
    private String billingAddress;
    private Date shipDate;

    public UpdateOrderRequest(String shipAddress, String billingAddress, Date shipDate) {
        this.shipAddress = shipAddress;
        this.billingAddress = billingAddress;
        this.shipDate = shipDate;
    }

    public UpdateOrderRequest() {
    }

    public String getShipAddress() {
        return shipAddress;
    }

    public void setShipAddress(String shipAddress) {
        this.shipAddress = shipAddress;
    }

    public String getBillingAddress() {
        return billingAddress;
    }

    public void setBillingAddress(String billingAddress) {
        this.billingAddress = billingAddress;
    }

    public Date getShipDate() {
        return shipDate;
    }

    public void setShipDate(Date shipDate) {
        this.shipDate = shipDate;
    }
}
