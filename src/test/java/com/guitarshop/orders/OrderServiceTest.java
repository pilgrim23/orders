package com.guitarshop.orders;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Optional;

import static org.junit.jupiter.api.Assertions.*;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;
import static org.mockito.ArgumentMatchers.*;

@ExtendWith(MockitoExtension.class)
public class OrderServiceTest {

    @Mock
    OrdersRepository ordersRepository;

    private OrdersService ordersService;
    private List<Order> testOrders;

    @BeforeEach
    void setup() {
        ordersService = new OrdersService(ordersRepository);
        testOrders = new ArrayList<>();
        for (int i = 0; i < 5; i++) {
            Order order = new Order(1, 2.5, 1.72, "1 E Main, Maltby, WA 00000");
            testOrders.add(order);
        }
    }

    @Test
    void getOrders_Blank_ReturnsAllOrders() {
        when(ordersRepository.findAll()).thenReturn(testOrders);

        OrdersList ordersList = ordersService.getOrders();
        assertNotNull(ordersList);
        assertEquals(testOrders.size(), ordersList.getOrders().size());
    }

    @Test
    void getOrders_Blank_ReturnsNull() {
        when(ordersRepository.findAll()).thenReturn(new ArrayList<>());

        OrdersList ordersList = ordersService.getOrders();
        assertNull(ordersList);
    }

    @Test
    void addOrder_Order_ReturnNewOrder() {
        Order order = testOrders.get(0);
        when(ordersRepository.save(any(Order.class))).thenReturn(order);
        Order actual = ordersService.addOrder(order);
        assertNotNull(actual);
        assertEquals(order.getCustomerId(), actual.getCustomerId());
    }

    @Test
    void getOrder_OrderId_ReturnsFoundOrder() {
        Order order = testOrders.get(0);
        when(ordersRepository.findById(anyLong())).thenReturn(Optional.of(order));
        Order actual = ordersService.getOrderById(order.getOrderId());
        assertNotNull(actual);
        assertEquals(order.getCustomerId(), actual.getCustomerId());
    }

    @Test
    void getOrder_OrderId_ReturnsNull() {
        Order order = testOrders.get(0);
        when(ordersRepository.findById(anyLong())).thenReturn(Optional.empty());
        Order actual = ordersService.getOrderById(order.getOrderId());
        assertNull(actual);
    }

    @Test
    void updateOrder_OrderId_returnUpdatedOrder() {
        Order order = testOrders.get(0);
        when(ordersRepository.findById(anyLong())).thenReturn(Optional.of(order));
        when(ordersRepository.save(any(Order.class))).thenReturn(order);

        Order actual = ordersService.updateOrderById(order.getOrderId(), "ship", "bill", new Date(0));
        assertNotNull(actual);
        assertEquals(order.getCustomerId(), actual.getCustomerId());
    }

    @Test
    void updateOrder_OrderId_returnsNull() {
        Order order = testOrders.get(0);
        when(ordersRepository.findById(anyLong())).thenReturn(Optional.empty());
        Order actual = ordersService.updateOrderById(order.getOrderId(), "ship", "bill", new Date(0));
        assertNull(actual);
    }

    @Test
    void deleteOrder_OrderId_Accepted() {
        Order order = testOrders.get(0);
        when(ordersRepository.findById(anyLong())).thenReturn(Optional.of(order));
        ordersService.deleteOrder(order.getOrderId());
        verify(ordersRepository).delete(any(Order.class));
    }

    @Test
    void deleteOrder_OrderId_NotFound() {
        when(ordersRepository.findById(anyLong())).thenReturn(Optional.empty());
        assertThrows(OrderNotFoundException.class, () -> ordersService.deleteOrder(10000L));
    }
}
